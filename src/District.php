<?php

/**
 * 中国行政区划查询
 *
 * Author: Lan Bing Hui <tabalus@qq.com>
 * Copyright (c) 2008-2018 IICGO Group. All rights reserved.
 *
 */

namespace IIcgo\District;

class District
{
    public $pcns;
    public $filter;
    public $cache = false;

    private $provinceMark = '|';
    private $cityMark     = ':';
    private $countryMark  = '.';
    private $townMark     = '?';
    private $villageMark  = ',';
    private $nameMark     = ',';

    public function setData()
    {
        if (!$this->cache) {
            $this->pcns = file_get_contents(__DIR__.'/../data/' . 'lastmonth.pcns');
            $this->filter = require __DIR__.'/../conf/' . 'filter.php';
        }
    }

    /**
     * 返回pcns数据和过滤文件表
     *
     */
    public function outData() : array
    {
        return [
            file_get_contents(__DIR__.'/../data/' . 'lastmonth.pcns'),
            require __DIR__.'/../conf/' . 'filter.php',
        ];
    }

    /**
     * 提供行政区划返回空格分隔的完整区域名称
     *
     * !!从左到右正确返回
     * 比如 $did = 3599 , 35是福建省, 99错误的区划，将返回：福建省
     *
     * @param  string district id  - 3501
     * @return string 完整区域名称或空字符
     *
     * Example:
     * district('350111202');
     *
     * Output: 空格分隔的完整区域名称
     * 福建省 福州市 晋安区 日溪乡
     *
     */
    public function district($did) : string
    {
        if (!ctype_digit($did)) {
            return '';
        }

        $pcns = $this->pcns;

        $Id = null;
        $ds = str_pad($did, 12, '0', STR_PAD_RIGHT);
        $itArray = ['province' => '', 'city' => '',  'country' => '', 'town' => '', 'village' => ''];

        $begin = 0;
        foreach ($itArray as $prop => $var) {
            ($prop == 'town' || $prop == 'village') ? $step = 3 : $step = 2;
            $Id[$prop] = substr($ds, $begin, $step);
            $begin += $step;
        }

        $propVar = null;
        foreach ($itArray as $prop => $var) {
            $propVar[$prop.'Name'] = '';
        }

        $offset  = 0;
        $befProp = '';
        foreach ($itArray as $prop => &$var) {
            if ($prop != 'village') {
                $fpid = strpos($pcns, $this->nameMark.$Id[$prop]. $this->{$prop.'Mark'}, $offset);
            } else {
                $fpid = strpos($pcns, $this->nameMark.$Id['village'], $offset);
            }

            if ($fpid === false) {
                break;
            }

            if ($befProp) {
                $pend = strpos($pcns, $this->{$befProp.'Mark'}, $offset);
                if ($fpid > $pend) {
                    break;
                }
            }

            ($prop == 'town' || $prop == 'village') ? $step = 5 : $step = 4;
            $prop != 'village' ? $offset = $fpid + $step : $offset = $fpid + 4;

            $nid = strpos($pcns, $this->nameMark, $offset);
            $length = $nid - $offset;
            $var = substr($pcns, $offset, $length);

            if ($prop == 'village') {
                if ($fpid === false) {
                    $offset = $fpid + $step;
                    $nid    = strpos($pcns, $this->nameMark, $offset);
                    $length = $nid - $offset;
                }
                break;
            }
            $befProp = $prop;
        }

        return trim(implode(' ', $itArray));
    }

    /**
     * 提供行政区划返回空格分隔的过滤的区域名称
     *
     * @param  string district id  - 3501
     * @param  string [low|middle|high]
     *
     *         low    更像地址,去掉自治等
     *         middle 像地址,但原子显示
     *         high   原子显示
     *
     * @return string 过滤的区域名称或空字符
     *
     * Example:
     *   district('350111202');
     * Output: 空格分隔的完整区域名称
     * 福建省 福州市 晋安区 日溪乡
     *
     */
    public function districtAb($did, $depth = 'low') : string
    {
        $address = $this->district($did);
        if (!$address) {
            return '';
        }

        $itArray = ['province' => '', 'city' => '',  'country' => '', 'town' => '', 'village' => ''];
        $searchArr = array_combine(array_keys($itArray), array_pad(explode(' ', $address), 5, ''));

        if ($depth == 'high') {
            $need = '';
            foreach ($searchArr as $prop => $var) {
                if (strlen($var) != 0) {
                    $need = $prop;
                }
            }
            return $this->filterAcme($searchArr[$need], $need);
        }

        foreach ($searchArr as $prop => &$var) {
            if (strlen($var) != 0) {
                if ($prop == 'city') {
                    if ($var == '市辖区'
                    || $var == '县'
                    || $var == '省直辖县级行政区划'
                    || $var == '自治区直辖县级行政区划') {
                        $var = '';
                    }
                }

                if ($depth == 'low') {
                    $var = $this->filterOrd($var, $prop);
                } else {
                    $var = $this->filterAcme($var, $prop);
                }
            }
        }

        if (mb_substr($searchArr['country'], -1) == '市') {
            if (mb_substr($searchArr['city'], -1) == '市') {
                $searchArr['city'] = mb_substr($searchArr['city'], 0, -1);
            }
        }

        return trim(implode(' ', $searchArr));
    }

    /**
     * 返回下一级的行政区划的数组
     *
     * $did 为空 ，则返回省id和名称
     * 没有下一级 bef.350111202210 ，返回空
     *
     * @param string $districtid  - bef. 3501|350102|350111202
     *
     * @return array|NULL - districtid => 'name'
     *
     * Example:
     * region('350111202');
     *
     * Output:
     * array(12) { [201]=> string(15) "日溪村委会" [202]=> string(15) "梓山村委会" ...
     *
     *
     */
    public function region($did = '')
    {
        $pcns   = $this->pcns;
        $outArr = null;
        $offset = 0;

        if (strlen($did) == 0) {
            while ($fpid = strpos($pcns, $this->provinceMark, $offset)) {
                $id  = substr($pcns, $fpid - 2, 2);
                if (strpos($id, ',') !== false) {
                    break;
                }

                $nid = strpos($pcns, $this->nameMark, $fpid);
                $outArr[$id] = substr($pcns, $fpid + 1, $nid - $fpid -1);
                $offset = $fpid + 1;
            }
            return $outArr;
        }

        $Id = null;
        $begin = 0;
        $itArray = ['province' => '', 'city' => '',  'country' => '', 'town' => '', 'village' => ''];

        foreach ($itArray as $prop => $var) {
            ($prop == 'town' || $prop == 'village') ? $step = 3 : $step = 2;
            $Id[$prop] = substr($did, $begin, $step) ?? '';
            $begin += $step;
        }

        foreach ($Id as $prop => $id) {
            if (strlen($id) == 0) {
                $fpid = strpos($pcns, $this->{$prop.'Mark'}, $offset);
                if ($prop == 'village') {
                    $fpid   = strpos($pcns, $this->{$prop.'Mark'}, $offset + 1);
                    $offset = $fpid;
                }

                $pend = strpos($pcns, $this->{$befProp.'Mark'}, $fpid + 5);
                foreach ($Id as $dis => $var) {
                    if (strlen($var) == 0) {
                        break;
                    }
                    $iend = strpos($pcns, $this->{$dis.'Mark'}, $fpid + 5);

                    if ($iend < $pend) {
                        $pend = $iend - 4;
                    }
                }

                if ($prop == 'village') {
                    $pend -= 5;
                }

                while ($fpid = strpos($pcns, $this->{$prop.'Mark'}, $offset)) {
                    if ($fpid > $pend) {
                        break;
                    }

                    ($prop == 'village' ) ? $startpos = 4 : $startpos = 1;
                    $offset = $fpid + $startpos;
                    $nid = strpos($pcns, $this->nameMark, $offset);
                    ($prop == 'town') ? $step = 4 : $step = 3;
                    ($prop == 'town' || $prop == 'village') ? $length = 3 : $length = 2;
                    $outArr[substr($pcns, $offset - $step, $length)] = substr($pcns, $offset, $nid - $offset);
                }

                break;
            }

            $fpid = strpos($pcns, $this->nameMark.$id.$this->{$prop.'Mark'}, $offset);

            if ($fpid === false) {
                break;
            }

            $offset  = $fpid;
            $befProp = $prop;
        }

        return $outArr;
    }

    /**
     * 根据给定的名字,返回区域号
     *
     * !! 结果跟输入的字符多少,长度有关,因此使用数组输出
     *
     * 如果返回数组为1, 就是精度结果
     *
     * @param Strings $name - 福州, 应输入空格表示层次   ,行政区号
     * @param Bool $contact - default = false ,如果有,是否给出上一级的至少完整名
     * @return array,false - 中间用 一个空格隔离,
     * [350100] = '福州市'
     *
     * Example:
     * search('福建')
     *
     * Output:
     * array(18) { [120103002001]=> string(70) "天津市 市辖区 河西区 下瓦房街道 福建路...
     * [35]=> string(13) "福建省 "
     *
     */
    public function search($name)
    {
        $return = false;
        $pcns   = $this->pcns;

        $number = substr_count($pcns, $name);
        if ($number == 0) {
            return $return;
        }

        $begin = 0;
        $offset = 0;
        $totallength = strlen($pcns);
        while ($begin < $number) {
            $pose           = strpos($pcns, $name, $offset);
            $behindpose     = strpos($pcns, $this->nameMark, $pose);
            $negativepose   = $pose - $totallength;
            $provincepose   = strrpos($pcns, $this->provinceMark, $negativepose);
            $citypose         = strrpos($pcns, $this->cityMark, $negativepose);
            $countrypose     = strrpos($pcns, $this->countryMark, $negativepose);
            $townpose         = strrpos($pcns, $this->townMark, $negativepose);
            $beforepose        = strrpos($pcns, $this->nameMark, $negativepose);
            $province_id    = substr($pcns, $provincepose-2, 2);
            $provinceEnd    = strpos($pcns, $this->nameMark, $provincepose);
            $provinceName   = substr($pcns, $provincepose+1, $provinceEnd-$provincepose-1);

            $cityName    = '';
            $countryName = '';
            $townName    = '';
            $villageName = '';

            if (!$citypose) {
                $citypose = 2;
            }

            if ($beforepose < $provincepose) {
                $city_id  = '';
                $country_id = '';
                $town_id = '';
                $village_id = '';
            } elseif ($beforepose < $citypose) {
                $city_id    = substr($pcns, $citypose-2, 2);
                $country_id = '';
                $town_id    = '';
                $village_id = '';

                $cityEnd  = strpos($pcns, $this->nameMark, $citypose);
                $cityName = substr($pcns, $citypose+1, $cityEnd - $citypose-1);
            } elseif ($beforepose < $countrypose) {
                $city_id    = substr($pcns, $citypose-2, 2);
                $country_id = substr($pcns, $countrypose-2, 2);
                $town_id    = '';
                $village_id = '';
                $cityEnd    = strpos($pcns, $this->nameMark, $citypose);
                $cityName   = substr($pcns, $citypose+1, $cityEnd-$citypose-1);

                $countryEnd  = strpos($pcns, $this->nameMark, $countrypose);
                $countryName = substr($pcns, $countrypose+1, $countryEnd-$countrypose-1);
            } elseif ($beforepose < $townpose) {
                $city_id    = substr($pcns, $citypose - 2, 2);
                $country_id = substr($pcns, $countrypose - 2, 2);
                $town_id    = substr($pcns, $townpose - 3, 3);
                $village_id = '';
                $cityEnd    = strpos($pcns, $this->nameMark, $citypose);
                $cityName   = substr($pcns, $citypose + 1, $cityEnd - $citypose-1);

                $countryEnd  = strpos($pcns, $this->nameMark, $countrypose);
                $countryName = substr($pcns, $countrypose+1, $countryEnd - $countrypose-1);

                $townEnd  = strpos($pcns, $this->nameMark, $townpose);
                $townName = substr($pcns, $townpose+1, $townEnd - $townpose-1);
            } else {
                $city_id    = substr($pcns, $citypose - 2, 2);
                $country_id = substr($pcns, $countrypose - 2, 2);
                $town_id    = substr($pcns, $townpose - 3, 3);
                $village_id = substr($pcns, $beforepose + 1, 3);

                $cityEnd  = strpos($pcns, $this->nameMark, $citypose);
                $cityName = substr($pcns, $citypose + 1, $cityEnd - $citypose-1);

                $countryEnd  = strpos($pcns, $this->nameMark, $countrypose);
                $countryName = substr($pcns, $countrypose + 1, $countryEnd - $countrypose-1);

                $townEnd  = strpos($pcns, $this->nameMark, $townpose);
                $townName = substr($pcns, $townpose + 1, $townEnd - $townpose-1);

                $villageName = substr($pcns, $beforepose + 4, $behindpose - $beforepose - 4);
            }

            $district = $province_id.$city_id.$country_id.$town_id.$village_id;
            $fullname = $provinceName.' '.$cityName.' '.$countryName.' '.$townName.' '.$villageName;

            $return[$district] = $fullname;
            $offset = $behindpose;

            $begin++;
        }
        return $return;
    }

    /**
     * 根据给定的名字,返回区域号
     *
     * @param strings $name - 要从完整的地区名从省开始，如果不带地区特征，就用空格
     * @return int|null     - 返回完整的区域名称
     *
     * Example:
     * 福州 晋安 日溪 日溪
     * 福建省福州市
     * 福建省 福州市
     * 福建 福州
     * 福建省
     *
     * Output: 35 |3501 |350102
     *
     */
    public function searchDistrict($name)
    {
        if (is_numeric($name)) {
            return null;
        }

        $name     = $this->filterDistrict($name);
        $disname  = explode(' ', $name);
        $district = null;
        $beforedis= null;
        foreach ($disname as $region) {
            if (!$region) {
                continue;
            }

            if ($beforedis) {
                $regs = $this->region($beforedis);
                if ($regs) {
                    foreach ($regs as $dis => $standardname) {
                        if (strpos($standardname, $region) !== false) {
                            $beforedis .= $dis;
                            break;
                        }
                    }
                }
            } else {
                $match = $this->search($region);
                if ($match) {
                    ksort($match, SORT_NUMERIC);
                    $beforedis = key($match);
                }
            }
        }

        return $beforedis;
    }

    /**
     * 过滤所有的区域名称，替换为空格
     *
     * @param string  福建省福州市鼓楼区
     * @return string 福建 福州 鼓楼
     *
     */
    public function filterDistrict(string $name) : string
    {
        return trim(str_replace($this->filter['district'], ' ', $name));
    }

    private function filterAcme($name, $node = 'city')
    {
        $filter = $this->filter['acme'][$node];
        return str_replace(array_keys($filter), array_values($filter), $name);
    }

    private function filterOrd($name, $node = 'city')
    {
        $filter = $this->filter['ord'][$node];
        return str_replace(array_keys($filter), array_values($filter), $name);
    }
}
